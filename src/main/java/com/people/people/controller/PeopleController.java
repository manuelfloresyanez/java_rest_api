package com.people.people.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.people.people.model.People;
import com.people.people.repository.PeopleRepository;

@RestController
@RequestMapping("/people")
public class PeopleController {

	@Autowired
	private PeopleRepository peopleRepository;

	@GetMapping()
	public ResponseEntity<?> findAllPeople() {
		Map<String, Object> response = new HashMap<>();
		List<People> people = this.peopleRepository.findAll();
		if(people!=null) {
			return ResponseEntity.ok().body(people);
		}else {
			response.put("message:", "Error finding People");
			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("{id}")
	public ResponseEntity<?> findPeople(@PathVariable(value = "id") UUID id) {
		People people = this.peopleRepository.findById(id).orElse(null);
		if (people != null) {
			return new ResponseEntity<>(people, HttpStatus.OK);
		} else {
			Map<String, Object> response = new HashMap<>();
			response.put("message:", "Invalid Id");
			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping()
	public ResponseEntity<?> createPeople(@RequestBody People people) {
		Map<String, Object> response = new HashMap<>();
		people.setId(UUID.randomUUID());
		if (!StringUtils.isEmpty(people.getRut()) && !StringUtils.isEmpty(people.getName())
				&& !StringUtils.isEmpty(people.getLastname())) {
			if (!StringUtils.isEmpty(people.getAge()) && (Integer.parseInt(people.getAge()) >= 18)) {
				People peopleCreated = this.peopleRepository.save(people);
				if (peopleCreated != null) {
					response.put("message:", "Person successfully Created");
					response.put("_id:", peopleCreated.getId());
					return new ResponseEntity<>(response, HttpStatus.OK);
				} else {
					response.put("message:", "Error saving Person");
					return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
				}
			} else if (!StringUtils.isEmpty(people.getAge())) {
				response.put("message:", "Person must be at least 18 years old");
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
			}
		}
		response.put("message:", "Invalid Data, please enter all the required fields");
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

	@PutMapping("{id}")
	public ResponseEntity<?> updatePeople(@RequestBody People peopleReceived, @PathVariable("id") UUID id) {
		Map<String, Object> response = new HashMap<>(); 
		People people = this.peopleRepository.findById(id).orElse(null);
		if (people != null) {
			if (!StringUtils.isEmpty(peopleReceived.getRut()) || !StringUtils.isEmpty(peopleReceived.getName())
					|| !StringUtils.isEmpty(peopleReceived.getLastname())
					|| !StringUtils.isEmpty(peopleReceived.getCourse())
					|| (!StringUtils.isEmpty(peopleReceived.getAge())
							&& (Integer.parseInt(peopleReceived.getAge()) >= 18))) {

				if (!StringUtils.isEmpty(peopleReceived.getRut())) {
					people.setRut(peopleReceived.getRut());
				}
				if (!StringUtils.isEmpty(peopleReceived.getName())) {
					people.setName(peopleReceived.getName());
				}
				if (!StringUtils.isEmpty(peopleReceived.getLastname())) {
					people.setLastname(peopleReceived.getLastname());
				}
				if (!StringUtils.isEmpty(peopleReceived.getCourse())) {
					people.setCourse(peopleReceived.getCourse());
				}

				if (!StringUtils.isEmpty(peopleReceived.getAge())
						&& (Integer.parseInt(peopleReceived.getAge()) >= 18)) {
					people.setAge(peopleReceived.getAge());
				} else if (!StringUtils.isEmpty(peopleReceived.getAge())
						&& (Integer.parseInt(peopleReceived.getAge()) < 18)) {
					response.put("message:", "Person must be at least 18 years old");
					return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
				}
				People peopleUpdated = this.peopleRepository.save(people);
				if (peopleUpdated != null) {
					response.put("message:", "Person succesfully updated");
					response.put("_id:", peopleUpdated.getId());
					return new ResponseEntity<>(response, HttpStatus.OK);
				} else {
					response.put("message:", "Error updating Person");
					return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
			response.put("message:", "Invalid Data, please enter at least one of the required fields");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		response.put("message:", "Id not found");
		return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
	}

	@DeleteMapping("{id}")
	public ResponseEntity<?> deletePeople(@PathVariable("id") UUID id) throws Exception {
		People people = this.peopleRepository.findById(id).orElse(null);
		if (people != null) {
			try {
				this.peopleRepository.deleteById(id);
				Map<String, Object> response = new HashMap<>();
				response.put("message:", "Person successfully deleted");
				response.put("_id:", id);
				return new ResponseEntity<>(response, HttpStatus.OK);
			} catch (Error e) {
				throw new Exception("Error when trying to delete Person");
			}
		} else {
			Map<String, Object> response = new HashMap<>();
			response.put("message:", "Id not found");
			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}
	}
}
